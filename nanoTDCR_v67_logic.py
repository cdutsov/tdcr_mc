#!/bin/python
import sys
from datetime import datetime
import pandas as pd

import subprocess

if len(sys.argv) == 1:
    filename = ''
else:
    filename = sys.argv[1]

A = int(1)
B = int(2)
C = int(4)
AB = int(3)
AC = int(5)
BC = int(6)
ABC = int(7)
D = int(0)
S = int(9)
CWA = int(11)
CWB = int(12)
CWC = int(14)
DTA = int(21)
DTB = int(22)
DTC = int(24)

tsa = 0
tsb = 0
tsc = 0

extended_dt = 50000 * 1e3  # ps
coincidence_window = 100 * 1e3  # ps

current_cw = {A: 0, B: 0, C: 0}
current_dt = {A: 0, B: 0, C: 0}
cw_was_open = {A: False, B: False, C: False}
dt_was_open = {A: False, B: False, C: False}
cw_is_open = {A: False, B: False, C: False}
dt_is_open = {A: False, B: False, C: False}

current_time = 0
raw_ts = 0
show_time_dist = False

total_coinc = {A: 0, B: 0, C: 0, AB: 0, BC: 0, AC: 0, ABC: 0, D: 0, S: 0}
coinc = {A: 0, B: 0, C: 0, AB: 0, BC: 0, AC: 0, ABC: 0, D: 0, S: 0}

dead_time_start = {A: 0, B: 0, C: 0, AB: 0, AC: 0, BC: 0, ABC: 0}
live_time = 0
dead_time = {A: 0, B: 0, C: 0, AB: 0, AC: 0, BC: 0, ABC: 0, D: 0}
dead_window = 0
pmts = [A, B, C, AB, AC, BC, ABC, D, S]
singles = [A, B, C]
doubles = [AB, AC, BC]
doubles_triples = [AB, AC, BC, ABC, D, S]
cws = [CWA, CWB, CWC]
dts = [DTA, DTB, DTC]

# pulse_arrival_tmp = [[0] * (coincidence_window + 1),
#                      [0] * (coincidence_window + 1),
#                      [0] * (coincidence_window + 1)]
# pulse_arrival = [[0] * (coincidence_window + 1),
#                  [0] * (coincidence_window + 1),
#                  [0] * (coincidence_window + 1)]

start_time = datetime.now()

real_time = []

ts = {}
ts_prev = {}

signals = {}


def add_coincidences(sig):
    coinc[AB] = int(coinc[A] and coinc[B] and (sig == A or sig == B))
    coinc[BC] = int(coinc[B] and coinc[C] and (sig == B or sig == C))
    coinc[AC] = int(coinc[A] and coinc[C] and (sig == A or sig == C))
    coinc[ABC] = coinc[A] and coinc[B] and coinc[C]
    if not coinc[ABC]:
        coinc[D] = coinc[AB] or coinc[AC] or coinc[BC]
    coinc[S] = coinc[A] or coinc[B] or coinc[C]

    # c = sum([c for c in coinc.values()])
    # print("A\tB\tC\tAB\tBC\tAC\tT\tD\tS\n")
    # for c, v in coinc.items():
    #     print(v, end="\t")
    # print("\n")
    # input()
    # if(coinc[ABC] == 1):
    #     print("ABC coinc:", raw_ts)

    total_coinc[sig] += 1
    for p in doubles_triples:
        total_coinc[p] += coinc[p]

        # if show_time_dist and coinc[ABC] == 1 and c == 8 and p < 5:
        #     for i in range(0, coincidence_window + 1):
        #         ind = int(p / 2)
        #         pulse_arrival[ind][i] += pulse_arrival_tmp[ind][i]
        #         pulse_arrival_tmp[ind][i] = 0

    for p in pmts:
        coinc[p] = 0


def print_output_rates(live_time, real_time, dead_window):
    time_passed = (datetime.now() - start_time).total_seconds()
    print("\nTotal time: " + str(time_passed) + " seconds")
    for field in ['A    ', 'B   ', 'C   ', 'AB  ', 'BC  ', 'AC  ', 'T   ',
                  'D    ', 'S     ', 'cpsA', 'cpsB', 'cpsC', 'cpsAB',
                  'cpsBC', 'cpsAC', 'cpsT', 'cpsD', 'T/D']:
        print(field, end='\t')
    print('\n', end='')
    for pmt in pmts:
        print(total_coinc[pmt], end='\t')
    cnt_rate = {}

    ab_bc_ac_lt = [total_coinc[key] / (real_time - dead_time[key]) for
                   key in doubles]
    doubles_lt = sum(ab_bc_ac_lt)
    triples_lt = (total_coinc[ABC]) / (real_time - dead_time[ABC])
    logical_d_lt = (total_coinc[D] / (doubles_lt - 2*triples_lt))
    dead_time[D] = real_time - logical_d_lt
    for key, value in total_coinc.items():
        if not key == S:
            cnt_rate[key] = value / (real_time - dead_time[key]) * 1e12
            print(round(cnt_rate[key], 4), end="\t")
    # logical_d_cr = sum([cnt_rate[key] for key in doubles]) - 2*cnt_rate[ABC]
    # print(round(logical_d_cr, 4), end="\t")
    print(round(cnt_rate[ABC] / cnt_rate[D], 5), end="\n")
    print('')
    for field in ['CS1  ', 'CS2 ', 'LiveTime', 'DeadTIme',
                  'DT%', 'RealTime', 'CWtimes', 'DT+LT+CW']:
        print(field, end='\t')
    print('')

    print(total_coinc[AB] +
          total_coinc[BC] +
          total_coinc[AC] -
          2 * total_coinc[ABC] - total_coinc[D], end='\t\t')
    print(total_coinc[S] +
          total_coinc[D] +
          total_coinc[ABC] -
          total_coinc[A] - total_coinc[B] - total_coinc[C], end='\t\t')
    print(live_time / 1e12, end="\t")
    print(round(dead_time[0] / real_time * 100, 5), end="\t")
    print(real_time / 1e12, end="\t")
    print(dead_window / 1e12, end="\t")
    print("Dead times: ", [round(dt / 1e12, 5) for k, dt in dead_time.items()])


def print_progress(cp):
    if cp % 10000 == 0:
        pass
        # print("\rCycles passed: ", cp, end="")
        # print(dead_time.items())


def sort_file(fname):
    print("Sorting file... ", fname)
    df = pd.read_csv(
        fname,
        sep=" ",
        skipinitialspace=True,
        skiprows=0,
        names=[
            'ts',
            'ev_t',
            'decay_t',
            'pmt',
            'n_d',
            'n_n',
            'n_a'])
    df.sort_values('ts', inplace=True)
    # print("File sorted!")
    return df


def get_real_time():
    real_time = 0
    with open(filename) as f:
        # print(events[-1])
        real_time = (float(subprocess.check_output(
            ['tail', '-1', f.name]).split()[0].decode('utf-8')))
        # max_events = sum(events) * 2  # CW and DT correction
        return real_time * 1e12


def main_loop():
    global raw_ts
    cycles_passed = 0
    index = 2
    common_cw_end = 0
    current_time = 0

    df = sort_file(filename)
    # iterrows = df.iterrows()
    # while iterrows:
    i = 0
    while i < df.shape[0]:
        cycles_passed += 1
        print_progress(cycles_passed)

        # Decide when to buffer another signal
        buffered_signals = []
        for ts, signal in signals.items():
            buffered_signals.append(signal)
        if 0 <= index < 3 or len(signals) < 1 or min(buffered_signals) > 4:
            index = int(df.at[i, 'pmt']) - 1
            if index == -1:
                i += 1
                continue
            pmt = 2**index
            ts = float(df.at[i, 'ts']) * 1e9
            timestamp = int(ts) * 1e3 + pmt
            raw_ts = int(ts)
            signals.update({timestamp: pmt})
            i += 1

        signals_sorted_keys = sorted(signals)
        current_time = signals_sorted_keys[0]
        pmt = int(signals[current_time])
        # Fix possibility of 2 photons in same ns
        index = int(pmt / 2)

        # print(current_time, pmt, dt_is_open, dead_time_start)
        # print(current_time, pmt, index, len(signals))
        # print(pmt, signals)

        if pmt < 5:  # signal comes from PMT
            # skip signal if cw is open
            if cw_is_open[pmt]:
                del signals[current_time]
                continue

            if not dt_is_open[pmt]:
                # Open CW and start dead-time
                if cw_is_open[A] or cw_is_open[B] or cw_is_open[C]:
                    cw_end = common_cw_end + pmt + 10
                else:
                    cw_end = current_time + coincidence_window + pmt + 10
                    common_cw_end = cw_end

                cw_is_open[pmt] = True
                signals.update({cw_end: pmt + 10})

                # track start of dead time
                for p in [A, B, C, AB, BC, AC, ABC]:
                    if not (p & pmt == 0) and dead_time_start[p] == 0:
                        dead_time_start[p] = current_time

                dt_end = current_time + extended_dt + pmt + 20
                signals.update({dt_end: pmt + 20})
                dt_is_open[pmt] = True

            else:
                dt_end = current_time + extended_dt + pmt + 20
                timestamps_to_delete = []
                for ts, sig in signals.items():
                    if sig == pmt + 20:
                        # signals[dt_end] = signals.pop(ts)
                        timestamps_to_delete.append(ts)
                for ts in timestamps_to_delete:
                    del signals[ts]

                signals.update({dt_end: pmt + 20})

        elif 5 < pmt < 15:  # signal comes from coincidence window end
            signal = pmt - 10

            for cw, is_open in cw_is_open.items():
                if is_open:
                    coinc[cw] += 1
            add_coincidences(signal)
            cw_is_open[signal] = False

        elif 15 < pmt < 25:  # signal comes from dead time end
            signal = pmt - 20
            dt_is_open[signal] = False

            # add dead time of single A, B or C
            # dt = (current_time - dead_time_start[signal])
            # print("DEAD TIME END T", dt)
            # if (dt > 200000):
            #     exit(1)
            dead_time[signal] += current_time - dead_time_start[signal]

            # add dead time of doubles
            for s in singles:
                if not s == signal and dead_time_start[s] == 0:
                    dt_sig = s + signal
                    dt_extended = current_time - dead_time_start[dt_sig]
                    dead_time[dt_sig] += dt_extended
                    dead_time_start[dt_sig] = 0

            dead_time_start[signal] = 0

            # add dead time of triples ABC
            if dead_time_start[A] == 0 \
                    and dead_time_start[B] == 0 \
                    and dead_time_start[C] == 0 \
                    and not dead_time_start[ABC] == 0:
                dead_time[ABC] += current_time - dead_time_start[ABC]
                # print(int(dead_time[ABC]/1000), dead_time_start[ABC])
                dead_time_start[ABC] = 0

        if current_time in signals.keys():
            del signals[current_time]

    real_time = get_real_time()
    print_output_rates(live_time, real_time, dead_window)


main_loop()
