#!/bin/bash 

# First argument is the input file directory
input_files_dir=$1

# Second argument is the output file name
outprefix=$2

cp ./MC_TDCR_Fortran_v2/MC_TDCR_v2.x .

run_simulation() {

    input_file=$1
    output_file_name="mac_"$2
    output_file_name_nano="nano_"$2
    i=$3

    cp $input_files_dir/$input_file "./input.txt"

    seed1=`shuf -i 1-99999 -n 1`
    seed2=`shuf -i 1-99999 -n 1`

    pulses_name="P_"$i`echo $input_file | cut -c 6-11`".dat"
    stats_name="S_"$i`echo $input_file | cut -c 6-11`".dat"

    echo $pulses_name $stats_name

    sed -i "46s/.*/$seed1/"       input.txt
    sed -i "47s/.*/$seed2/"       input.txt
    sed -i "53s/.*/$pulses_name/" input.txt
    sed -i "54s/.*/$stats_name/"  input.txt

    echo "Processing file" $input_file 

    # Run MC code
    ./MC_TDCR_v2.x

    mv $pulses_name $input_files_dir/$pulses_name
    mv $stats_name  $input_files_dir/$stats_name

    ./MAC3_TDCR_logic.py $input_files_dir/$pulses_name >> $input_files_dir/$output_file_name
    ./nanoTDCR_v67_logic.py $input_files_dir/$pulses_name >> $input_files_dir/$output_file_name_nano

    echo "Processed file:" $input_file " run:" $i 
    echo "Data written to " $outfile
    rm -f $input_files_dir/$pulses_name
}

run_multiple() {
    input_files_dir=$1
    i=$3

    for input in `ls $input_files_dir/input*`
    do
        input_file=`echo ${input##*/}`
        outfile=$2`echo $input_file | cut -c 6-11`".dat"
        echo $input_file $outfile
        echo "Doing run" $i "of file " $input_file
        run_simulation $input_file $outfile $i &
        sleep 5
    done
}

run_multiple $input_files_dir $outprefix 1

# for i in {1..10}
# do
#     run_multiple $input_files_dir $outprefix $i
    
#     sleep 10
#     while [ `pgrep TDCR | wc -l` -ne 0 ] 
#     do
#         sleep 10
#     done
# done
