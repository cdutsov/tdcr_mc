extern crate rand;
use rand::{thread_rng, Rng};
use std::env;
use std::io::{self, BufRead};

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Enter filter value between 1 and 0");
        std::process::exit(1);
    }
    let filter_value = args[1].parse::<f64>().unwrap();
    if filter_value > 1.0 || filter_value < 0.0 {
        println!("Enter valid filter value between 1 and 0!");
        std::process::exit(1);
    }
    let mut input = String::new();
    let mut rand = rand::thread_rng();

    loop {
        let next_line = match io::stdin().lock().lines().next() {
            Some(line) => line.unwrap(),
            None => String::from("EOF"),
        };
        if next_line == "EOF" {
            std::process::exit(1);
        }
        input = next_line;

        if rand.gen_range(0.0, 1.0) < filter_value {
            println!("{}", input);
        }
    }
}
