#!/bin/python3
import sys
from datetime import datetime
import pandas as pd
import subprocess

if len(sys.argv) == 1:
    filename = "../TDCR_MC/first_results/PulsesTDCR.dat"
else:
    filename = sys.argv[1]

A = int(1)
B = int(2)
C = int(4)
AB = int(3)
AC = int(5)
BC = int(6)
ABC = int(7)
D = int(0)
S = int(9)

extended_dt = 50000 * 1e3
coincidence_window = 100 * 1e3
show_time_dist = True

tot_coinc = {A: 0, B: 0, C: 0, AB: 0, BC: 0, AC: 0, ABC: 0, D: 0, S: 0}
coinc = {A: 0, B: 0, C: 0, AB: 0, BC: 0, AC: 0, ABC: 0, D: 0, S: 0}

live_time = 0
dead_window = 0
dead_time = {0: 0, A: 0, B: 0, C: 0, AB: 0, AC: 0, BC: 0, ABC: 0}
pmts = [A, B, C, AB, BC, AC, D, ABC, S]

bins = int(coincidence_window / 1e3)
pulse_arrival_tmp = [[0] * (bins + 1),
                     [0] * (bins + 1),
                     [0] * (bins + 1)]
pulse_arrival = [[0] * (bins + 1),
                 [0] * (bins + 1),
                 [0] * (bins + 1)]

start_time = datetime.now()

ts = {}
ts_prev = {}
signals = {}
raw_ts = 0


def add_coincidences():
    coinc[AB] = coinc[A] and coinc[B]
    coinc[BC] = coinc[B] and coinc[C]
    coinc[AC] = coinc[A] and coinc[C]
    coinc[ABC] = coinc[A] and coinc[B] and coinc[C]
    coinc[D] = coinc[AB] or coinc[AC] or coinc[BC]
    coinc[S] = coinc[A] or coinc[B] or coinc[C]

    for p in pmts:
        tot_coinc[p] += coinc[p]
        if show_time_dist and p < 5:
            if coinc[D] == 1 and not coinc[ABC] == 1:
                for i in range(0, bins + 1):
                    pulse_arrival[int(
                        p / 2)][i] += pulse_arrival_tmp[int(p / 2)][i]
                    pulse_arrival_tmp[int(p / 2)][i] = 0

        coinc[p] = 0


def get_counting_rates(tot_coinc, real_time, dead_time, dead_window):
    counting_rates = {}
    for key, value in tot_coinc.items():
        if not key == S:
            counting_rates[key] = value / (
                real_time - dead_time[0] - dead_window) * 1e12
    return counting_rates


def print_output_rates(live_time, real_time, dead_window):
    time_passed = (datetime.now() - start_time).total_seconds()
    print("\nTotal time: " + str(time_passed) + " seconds")
    for field in ['A    ', 'B   ', 'C   ', 'AB  ', 'BC  ', 'AC  ', 'D   ',
                  'T    ', 'S     ', 'cpsA', 'cpsB', 'cpsC', 'cpsAB',
                  'cpsBC', 'cpsAC', 'cpsT', 'cpsD', 'T/D']:
        print(field, end='\t')
    print('\n', end='')
    for pmt in pmts:
        print(tot_coinc[pmt], end='\t')

    counting_rates = get_counting_rates(
        tot_coinc, real_time, dead_time, dead_window)
    for key, value in counting_rates.items():
        print(round(counting_rates[key], 4), end="\t")

    print(
        round(
            counting_rates[ABC] /
            (counting_rates[AB] + counting_rates[BC] +
                counting_rates[AC] - 2 * counting_rates[ABC]),
            5),
        end="\n")
    print('')
    for field in ['CS1', 'CS2', 'LiveTime', 'DeadTIme',
                  'DT%', 'RealTime', 'CWtimes', 'DT+LT+CW']:
        print(field, end='\t')

    print('')

    print(tot_coinc[AB] + tot_coinc[BC] + tot_coinc[AC] -
          2 * tot_coinc[ABC] - tot_coinc[D], end='\t')
    print(
        tot_coinc[S] + tot_coinc[D] + tot_coinc[ABC] - tot_coinc[A] -
        tot_coinc[B] - tot_coinc[C], end='\t\t')
    print(live_time / 1e12, end="\t")
    print(dead_time[0] / 1e12, end="\t")
    print(round(dead_time[0] / real_time * 100, 2), end="\t")
    print(real_time / 1e12, end="\t")
    print(dead_window / 1e12, end="\t")
    print((dead_time[0] + live_time + dead_window) / 1e12, end="\n")

    if show_time_dist:
        print("Distribution of pulses")
        print("PMTA\tPMTB\tPMTC")
        for i in range(0, bins):
            print(pulse_arrival[0][i],
                  pulse_arrival[1][i],
                  pulse_arrival[2][i])

    print("Total time: ", real_time / 1e12)
    print("----------------------------------------------------------")


def print_progress(cp):
    if cp % 10000 == 0:
        print("\rCycles passed: ", cp, end="")


def sort_file(fname):
    print("Sorting file...", fname)
    df = pd.read_csv(
        fname,
        sep=" ",
        skipinitialspace=True,
        skiprows=0,
        names=[
            'ts',
            'ev_t',
            'decay_t',
            'pmt',
            'n_d',
            'n_n',
            'n_a'])
    df.sort_values('ts', inplace=True)
    return df


def get_real_time():
    real_time = 0
    with open(filename) as f:
        real_time = (float(subprocess.check_output(
            ['tail', '-1', f.name]).split()[0].decode('utf-8')))
        # max_events = sum(events) * 2  # CW and DT correction
        return real_time * 1e12


def main_loop():
    global raw_ts
    timestamp = 0
    index = 2

    cw_was_open = False
    cw_is_open = False
    dt_is_open = False

    current_dt = 0
    current_cw = 0
    current_time = 0

    dead_time_start = 0
    live_time_start = 0

    live_time_so_far = 0
    dead_window_so_far = 0

    cycles_passed = 0

    df = sort_file(filename)
    i = 0
    while i < df.shape[0]:
        cycles_passed += 1
        # print_progress(cycles_passed)

        buffered_signals = []
        for ts, signal in signals.items():
            buffered_signals.append(signal)
        if 0 <= index < 3 or len(signals) < 1 or min(buffered_signals) > 4:
            index = df.at[i, 'pmt'] - 1
            if index == -1:  # skip non-detected events; for now
                i += 1
                continue
            pmt = 2**index
            timestamp = int(float(df.at[i, 'ts']) * 1e9) * 1e3 + pmt
            raw_ts = timestamp
            signals.update({timestamp: pmt})
            i += 1

        signals_sorted_keys = sorted(signals)
        current_time = signals_sorted_keys[0]
        pmt = signals[current_time]
        index = int(pmt / 2)

        dt_is_open = (current_time < current_dt)
        cw_was_open = cw_is_open
        cw_is_open = (current_time < current_cw)

        if dt_is_open and current_dt in signals.keys():
            del signals[current_dt]
            current_dt = current_time + extended_dt + 5
            signals.update({current_dt: 10})
        elif not cw_was_open and not dt_is_open and index <= 3:
            cw_is_open = True
            coinc[pmt] = 1
            pulse_arrival_tmp[index][0] = 1
            current_cw = current_time + coincidence_window + 4
            signals.update({current_cw: 8})

            live_time_so_far += current_time - live_time_start
            dead_time_start = current_time
            current_dt = current_time + extended_dt + 5
        elif cw_was_open and cw_is_open and index <= 3:
            if show_time_dist and not coinc[pmt] == 1:
                pulse_arrival_tmp[index][
                    int((current_time - current_cw + coincidence_window)
                        / 1e3)] = 1
            coinc[pmt] = 1

        if index == 5 and current_dt in signals.keys():
            del signals[current_dt]
            dead_time[0] += current_time - dead_time_start
            live_time_start = current_time
        elif index == 4:
            signals.update({current_dt: 10})
            add_coincidences()

        if current_time in signals.keys():
            del signals[current_time]

    live_time = live_time_so_far
    dead_window = dead_window_so_far
    real_time = get_real_time()
    print_output_rates(live_time, real_time, dead_window)


main_loop()
